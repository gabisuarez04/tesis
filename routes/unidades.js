const express = require('express');
const router = express.Router();
const unidadesController = require('../controllers/unidades');

router.route('/')
	.get(unidadesController.getUnidades)
	.post(unidadesController.postUnidad);

router.route('/:id')
    .delete(unidadesController.deleteUnidad);

module.exports = router;
