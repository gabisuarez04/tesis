const express = require('express');
const router = express.Router();
const estacionesController = require('../controllers/estaciones');

router.route('/')
    .get(estacionesController.getEstaciones)
    .post(estacionesController.postEstacion);

router.route('/:id')
    .get(estacionesController.getEstacion)
    .put(estacionesController.putEstacion)
    .delete(estacionesController.deleteEstacion);
/*
router.use((req, res, next) => {
        console.log("Entro al ROUTE USE");
        next();
});
*/
module.exports = router;
