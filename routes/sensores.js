const express = require('express');
const router = express.Router();
const sensoresController = require('../controllers/sensores');

router.route('/')
	.get(sensoresController.getSensores)
	.post(sensoresController.postSensor);

router.route('/:id')
    .delete(sensoresController.deleteSensor);

module.exports = router;
