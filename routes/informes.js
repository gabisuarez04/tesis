const express = require('express');
const router = express.Router();
const informesController = require('../controllers/informes');

router.route('/')
    .get(informesController.getInformes)
    .post(informesController.postInforme);
    
router.route('/:id')
    .get(informesController.getInforme)
    .delete(informesController.deleteInforme);

module.exports = router;
