/* la variable de entorno NODE_ENV se usa para especificar si la aplicacion está en desarrollo o en produccion, 
y en base a eso se usan distintas configuraciones
*/

module.exports = require('./' + (process.env.NODE_ENV || 'development') + '.json');

