const express = require('express');
const app = express();
const morgan = require('morgan');
const mongoose = require('mongoose');
const config = require('./config');
const bodyParser = require('body-parser');
const conexiones = require('./db');

global.globalConexion = conexiones[1];

// Paquete de logs
app.use(morgan('dev'));

// Paquete para parsear bodys de los requests 
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());

// Definicion de rutas
const estacionesRoutes = require('./routes/estaciones');
const informesRoutes = require('./routes/informes');
const sensoresRoutes = require('./routes/sensores');
const unidadesRoutes = require('./routes/unidades');

app.use('/estaciones', estacionesRoutes);
app.use('/informes', informesRoutes);
app.use('/sensores', sensoresRoutes);
app.use('/unidades', unidadesRoutes);

// Manejador de request no definidos; si se llego a este punto es porque al request no lo puede manejar ningun router
app.use((req, res, next) => {
	const error = new Error('Not Found');
	error.status = 404;
	next(error); // Hace que se ejecute el manejador de errores
});

// Manejador de errores de toda la app
app.use((error, req, res, next) => {
	res.status(error.status || 500);
	res.json({
			error: {
				message: error.message
			}	
		}
	
	);
}

);

module.exports = app;



