const http = require('http');
const app = require('./app'); // la aplicacion express

const port = process.env.PORT || 3000;

const server = http.createServer(app);

server.listen(port);

console.log('Express esta corriendo en el puerto '+ port);
