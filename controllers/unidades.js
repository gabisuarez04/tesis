
const UnidadModel = require('../models/unidad');
const Unidad = UnidadModel.conexion2;

exports.getUnidades = function (req, res, next) { 
		console.log(globalConexion);
		Unidad.find()
			.exec()
			.then(unidades => {
				console.log(unidades);
				return res.json({unidades: unidades});
			})
			.catch(err => {
				console.log(err);
				return next(err);
			});
		
};

exports.postUnidad = function (req, res, next) { 

	var unidad = new Unidad({
		tipo: req.body.tipo		    
	});
    unidad.save()
			.then(unidad => {
				console.log('Unidad añadida con éxito');		
				return res.json({message: 'Unidad añadida', unidad: unidad});
			})
			.catch(err => {
				console.log('Error al añadir la unidad con tipo: ' + unidad.tipo);
				console.log("ERROR: " + err);
				return next(err);
			});
};


exports.deleteUnidad = function (req, res, next) {

	Unidad.findByIdAndRemove(req.params.id)
			.exec()
			.then(unidad => {
				if (!unidad){
					console.log('No existe una unidad con id:', req.params.id);		
					return res.json({message: 'No existe una unidad con id: '+req.params.id});
				}
				console.log('Unidad eliminada con éxito');		
				return res.json({message: 'Unidad eliminada', unidad: unidad});
			})
			.catch(err => {
				console.log('Error al eliminar la unidad con id:',req.params.id);
				return next(err); 
			});


};
