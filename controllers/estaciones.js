const Estacion = require('../models/estacion');

exports.getEstaciones = function (req, res, next) { 

		Estacion.find()
			.exec()
			.then(estaciones => {
				console.log(estaciones);
				return res.json({estaciones: estaciones});
			})
			.catch(err => {
				console.log(err);
				return next(err); /* la palabra return se usa para detener la ejecucion de la funcion; 
				si no se pone el callback puede que se ejecute más de una vez */
			});
		
};

exports.getEstacion = function (req, res, next) { 

	Estacion.findById(req.params.id)
		.exec()
		.then(estacion => {
			if (!estacion){
				console.log('No existe una estacion con id:', req.params.id);
				return res.json({message: 'No existe la estacion con id: '+req.params.id});
			}
			console.log(estacion);
			return res.json({estacion: estacion});
		})
		.catch(err => {
			console.log(err);
			return next(err);
		});
	
};


exports.postEstacion = function (req, res, next) { 

	var estacion = new Estacion({
		latitud: req.body.latitud,
		longitud: req.body.longitud 		    
	});
    estacion.save()
			.then(estacion => {
				console.log('Estacion añadida con éxito');		
				return res.json({message: 'Estacion añadida', estacion: estacion});
			})
			.catch(err => {
				console.log('Error al añadir la estacion');
				return next(err); 
			});
		
};

exports.putEstacion = function (req, res, next){
	estacion = new Estacion({
		latitud: req.body.latitud,
		longitud: req.body.longitud
	});
	Estacion.findByIdAndUpdate(req.params.id, estacion)
			.exec()
			.then( estacion => { 
					if (!estacion){
						console.log('No existe una estacion con id:', req.params.id);		
						return res.json({message: 'No existe la estacion con id: '+req.params.id});
					}
					console.log('Estacion actualizada con éxito');
					return res.json({message: 'Actualizacion exitosa de la Estacion con id: '+req.params.id});
			})
			.catch(err => {
				console.log('Error al actualizar la estacion:',req.params.id);
				return next(err); 
			});
};

exports.deleteEstacion = function (req, res, next) {

	Estacion.findByIdAndRemove(req.params.id)
			.exec()
			.then(estacion => {
				if (!estacion){
					console.log('No existe una estacion con id:', req.params.id);		
					return res.json({message: 'No existe la estacion con id: '+req.params.id});
				}
				console.log('Estacion eliminada con éxito');		
				return res.json({message: 'Estacion eliminada', estacion: estacion});
			})
			.catch(err => {
				console.log('Error al eliminar la estacion con id:',req.params.id);
				return next(err); 
			});


};

