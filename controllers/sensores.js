const Sensor = require('../models/sensor');

exports.getSensores = function (req, res, next) { 

		Sensor.find()
			.exec()
			.then(sensores => {
				console.log(sensores);
				return res.json({sensores: sensores});
			})
			.catch(err => {
				console.log(err);
				return next(err);
			});
		
};

exports.postSensor = function (req, res, next) { 

	var sensor = new Sensor({
		tipo: req.body.tipo		    
	});
    sensor.save()
			.then(sensor => {
				console.log('Sensor añadido con éxito');		
				return res.json({message: 'Sensor añadido', sensor: sensor});
			})
			.catch(err => {
				console.log('Error al añadir al sensor con tipo: ' + sensor.tipo);
				console.log("ERROR: " + err);
				return next(err);
			});
};

exports.deleteSensor = function (req, res, next) {

	Sensor.findByIdAndRemove(req.params.id)
			.exec()
			.then(sensor => {
				if (!sensor){
					console.log('No existe un sensor con id:', req.params.id);		
					return res.json({message: 'No existe un sensor con id: '+req.params.id});
				}
				console.log('Sensor eliminado con éxito');		
				return res.json({message: 'Sensor eliminado', sensor: sensor});
			})
			.catch(err => {
				console.log('Error al eliminar el sensor con id:',req.params.id);
				return next(err); 
			});


};
