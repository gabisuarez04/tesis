const Informe = require('../models/informe');

exports.getInformes = function (req, res, next) { 

        Informe.find()
            .populate('estacion')
			.exec()
			.then(informes => {
				console.log(informes);
				return res.json({informes: informes});
			})
			.catch(err => {
				console.log(err);
				return next (err);
			});
		
};

exports.getInforme = function (req, res, next) { 

	Informe.findById(req.params.id)
	.exec()
	.then(informe => {
		if (!informe){
			console.log('No existe un informe con id:', req.params.id);
			return res.json({message: 'No existe el informe con id: '+req.params.id});
		}
		console.log(informe);
		return res.json({informe: informe});
	})
	.catch(err => {
		console.log(err);
		return next(err);
	});


};


exports.postInforme = function (req, res, next) { 

/*
	for(var i = 0; i< req.body.mediciones.length; i++){
		var data = req.body.mediciones[i];
		arrayMediciones.push(data);
	};
*/
	var informe = new Informe({
		estacion: req.body.estacion,
		mediciones: req.body.mediciones
	});
	
	informe.save()
		.then(informe => {
				console.log('Informe añadido con éxito');		
				return res.json({message: 'Informe añadido', informe: informe});
		})
		.catch(err => {
				console.log('Error al añadir el informe');
				return next(err);
		});
	
};


exports.deleteInforme = function (req, res, next) {

	Informe.findByIdAndRemove(req.params.id)
			.exec()
			.then(informe => {
				if (!informe){
					console.log('No existe un informe con id:', req.params.id);		
					return res.json({message: 'No existe el informe con id: '+req.params.id});
				}
				console.log('Informe eliminado con éxito');		
				return res.json({message: 'Informe eliminado', informe: informe});
			})
			.catch(err => {
				console.log('Error al eliminar el informe con id:',req.params.id);
				return next(err); 
			});


};
