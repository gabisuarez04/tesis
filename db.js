const mongoose = require('mongoose');
const config = require('./config');
const arrayConexiones = [];

// Conexion a la base de datos

const options = {
	keepAlive: 10000,
//	poolSize: 10,
	connectTimeoutMS: 10000, // tiempo limite de espera para que se efectue la conexion
	socketTimeoutMS: 10000,
	connectWithNoPrimary: true // permite que se conecte a la replica por mas de que el primario no se encuentre disponible
}; /* en options se mantienen las configuraciones que son iguales en desarrollo o en produccion */

var urlConnection = config.Mongo.client + "://" + config.Mongo.host + ":" + config.Mongo.port + ",10.0.2.16,10.0.2.17"  + "/" + config.Mongo.dbName + "?replicaSet=" + config.Mongo.replSetName;
/* en la url se especifican las opciones que difieren en desarrollo y en produccion */

arrayConexiones.push(mongoose.createConnection(urlConnection,options));
arrayConexiones.push(mongoose.createConnection(urlConnection+"&readPreference=secondary&readPreferenceTags=miembro:2",options));


module.exports = arrayConexiones; 



