/* Para correr el script se debe ejecutar:

mongo insert.js

Los datos del set de replicas se pueden mandar por parámetro con el comando --eval:
mongo --eval "var hosts=hostReplicaSet, db= BD, replica=nombreReplica" insert.js

*/

var hosts='10.0.2.15,10.0.2.16,10.0.2.17',
    db = 'tesis',
    replica = 'rs0';

db = connect(hosts+"/"+db+"?replicaSet="+replica); 

// ------------- UNIDADES

db.unidades.insert([    { _id: 0, tipo: "grados fahrenheit" },
                        { _id: 1, tipo: "grados celsius" },
                        { _id: 2, tipo: "milimetros"},
                        { _id: 3, tipo: "porcentaje"}
                    ]);


// ------------- SENSORES

db.sensores.insert([    { _id: 0, tipo: "temperatura"},
                        { _id: 1, tipo: "precipitacion"},
                        { _id: 2, tipo: "humedad"}
                    ]);


// ------------- ESTACIONES

db.estaciones.insert([  { _id: 0, latitud: 25, longitud: 25},
                        { _id: 1, latitud: 30, longitud: 30},
                        { _id: 2, latitud: 35, longitud: 35},
                        { _id: 3, latitud: 40, longitud: 45},
                        { _id: 4, latitud: 45, longitud: 45},
                        { _id: 5, latitud: 50, longitud: 50},
                        { _id: 6, latitud: 55, longitud: 55},
                        { _id: 7, latitud: 60, longitud: 60},
                        { _id: 8, latitud: 65, longitud: 65},
                        { _id: 9, latitud: 70, longitud: 70}
                    ]);


// ------------- INFORMES

var inicio = (new Date).getTime();
var arrayInformes = [];
var id, informe;

for (var i = 0; i<= 16000000 ; i = i + 100000) {
    for (var j = 0; j <= 99999; j++) {
        id = i + j;
        informe = { _id: id, estacion: 2, fecha: new ISODate, 
                        mediciones:[  { _id: 0, sensor: 0, valor: 10, unidad: 1 },
                                      { _id: 1, sensor: 1, valor: 50, unidad: 2 },
                                      { _id: 2, sensor: 2, valor: 70, unidad: 3 }
                        ] 
                    };
        arrayInformes.push(informe);
    }
    
    db.informes.insert(arrayInformes);
    arrayInformes = [];
};

var tiempoTotal = (new Date).getTime() - inicio;
print('Tiempo de insercion de informes: '+ tiempoTotal +' milisegundos');

// -------------

/*
db.informes.find({fecha:{
    $gte:ISODate("2018-06-15T22:18:18.380Z")

}})

cursor = db.unidades.find();
while (cursor.hasNext()){
    printjson(cursor.next());
}


*/