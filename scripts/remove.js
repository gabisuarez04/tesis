/* Para correr el script se debe ejecutar:

mongo remove.js

Los datos del set de replicas se pueden mandar por parámetro con el comando --eval:
mongo --eval "var hosts=hostReplicaSet, db= BD, replica=nombreReplica" remove.js

*/

var hosts='10.0.2.15,10.0.2.16,10.0.2.17',
    db = 'tesis',
    replica = 'rs0';

db = connect(hosts+"/"+db+"?replicaSet="+replica); 

// ------------- UNIDADES

db.unidades.remove({});

// ------------- SENSORES

db.sensores.remove({});

// ------------- ESTACIONES

db.estaciones.remove({});

// ------------- INFORMES

db.informes.remove({});
