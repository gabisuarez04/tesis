const mongoose = require('mongoose');
const autoIncrement = require('mongoose-plugin-autoinc');
const medicionSchema = require('./medicion');

const informeSchema = mongoose.Schema(
{
	fecha: { type: Date, default: Date.now },
	estacion: { type: Number, ref: 'estacion'},
	mediciones: [ medicionSchema ]
	
}
);

informeSchema.plugin(autoIncrement.plugin, 'informe');
module.exports = mongoose.model('informe', informeSchema);