const mongoose = require('mongoose');

const medicionSchema = mongoose.Schema(
{
		sensor: { type: Number, ref: 'sensor'},
		valor: Number,
		unidad: { type: Number, ref: 'unidad' }

}
);

module.exports = medicionSchema;
