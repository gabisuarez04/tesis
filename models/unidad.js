const mongoose = require('mongoose');
const autoIncrement = require('mongoose-plugin-autoinc');
const conexion = globalConexion;

const unidadSchema = mongoose.Schema(
{
    tipo: String
}
);

unidadSchema.plugin(autoIncrement.plugin, 'unidad');
module.exports = { 
    conexion1: conexion.model('unidad', unidadSchema, 'unidades'),
    conexion2: conexion.model('unidad', unidadSchema, 'unidades') 
};


