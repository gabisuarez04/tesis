const mongoose = require('mongoose');
const autoIncrement = require('mongoose-plugin-autoinc');

const estacionSchema = mongoose.Schema(
{

	longitud: Number,
	latitud: Number
}
);

estacionSchema.plugin(autoIncrement.plugin, 'estacion');
module.exports = mongoose.model('estacion', estacionSchema, 'estaciones');

