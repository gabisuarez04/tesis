const mongoose = require('mongoose');
const autoIncrement = require('mongoose-plugin-autoinc');

const sensorSchema = mongoose.Schema(
{
    tipo: String
}
);

sensorSchema.plugin(autoIncrement.plugin, 'sensor');
module.exports = mongoose.model('sensor', sensorSchema, 'sensores');

