Para levantar el conjunto de réplicas:

1)  Ejecutar en cada servidor de base de datos:
sudo mongod --config /etc/archivo-mongod.conf

2)  Para verificar el estado del conjunto ejecutar en algún servidor la shell de Mongo
mongo --host unHostReplica 

    Dentro de la shell de Mongo ejecutar:
rs.status()

Para levantar el servidor de la api:

1) Posicionarse dentro del directorio del proyecto y ejecutar:
nodemon ./server.js


Para hacer un POST de informes se debe mandar en el body del request un json con el siguiente formato:

{
	"estacion": id_estacion,
	"mediciones": [
	
	{ "sensor": id_sensor,
	  "valor": numero,
	  "unidad": id_unidad
		
	},
	{ "sensor": id_sensor,
	  "valor": numero,
	  "unidad": id_unidad
		
	}
		
	]
	
}

Por el momento no se hacen validaciones de los parámetros:
- Mecanismos de seguridad y escrituras a la base no son el objetivo de la tesis 
- Proximamente se puede agregar middleware para validar (app.param o router.param)